package frc.robot;

import static frc.robot.Constants.OIConstants.*;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.commands.ArcadeDriveCmd;
import frc.robot.commands.TankDriveCmd;
import frc.robot.subsystems.DriveSubsystem;

public class RobotContainer {
  
  private final DriveSubsystem driveSubsystem = new DriveSubsystem();
  private final Joystick copilotController = new Joystick(COPILOT_JOYSTICK_PORT);
  private final Joystick driverController = new Joystick(DRIVER_JOYSTICK_PORT);

  public RobotContainer() {

    if (ARCADE_MODE) {
      ArcadeDriveCmd arcadeDriveCmd = new ArcadeDriveCmd(
          driveSubsystem,
          () -> driverController.getRawAxis(DRIVER_Y_AXIS),
          () -> -driverController.getRawAxis(DRIVER_X_AXIS)
      );
      driveSubsystem.setDefaultCommand(arcadeDriveCmd);
    } else {
      TankDriveCmd tankDriveCmd = new TankDriveCmd(
          driveSubsystem,
          () -> copilotController.getRawAxis(COPILOT_LEFT_Y_AXIS),
          () -> copilotController.getRawAxis(COPILOT_RIGHT_Y_AXIS)
      );
      driveSubsystem.setDefaultCommand(tankDriveCmd);
    }

    configureButtonBindings();

  }
  
  private Command configureButtonBindings() {
    return null;
  }
}